/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.shapeframe;

/**
 *
 * @author acer
 */
public class Rectangle extends Shape {

    private double Width;
    private double Height;

    public Rectangle(double Width, double Height) {
        super("Rectangle");
        this.Height = Height;
        this.Width = Width;

    }

    public double getWidth() {
        return Width;
    }

    public void setWidth(double Width) {
        this.Width = Width;
    }

    public double getHeight() {
        return Height;
    }

    public void setHeight(double Height) {
        this.Height = Height;
    }

    @Override
    public double calArea() {
        return Height * Width;
    }

    @Override
    public double calPerimeter() {
        return 2 * (Height + Width);
    }

}
