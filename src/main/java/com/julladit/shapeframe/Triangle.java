/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.shapeframe;

/**
 *
 * @author acer
 */
public class Triangle extends Shape {

    private double Base;
    private double Height;

    public Triangle(double Base, double Height) {
        super("Triangle");
        this.Base = Base;
        this.Height = Height;

    }

    public double getBase() {
        return Base;
    }

    public void setBase(double Base) {
        this.Base = Base;
    }

    public double getHeight() {
        return Height;
    }

    public void setHeight(double Height) {
        this.Height = Height;
    }

    @Override
    public double calArea() {
        return Base * Height * 0.5;
    }

    @Override
    public double calPerimeter() {
        return Height + Height + Base;
    }

}
