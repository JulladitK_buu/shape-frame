/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.shapeframe;

/**
 *
 * @author acer
 */
public abstract class Shape {

    private String Shapename;

    public Shape(String Shapename) {
        this.Shapename = Shapename;
    }

    public String getShapename() {
        return Shapename;
    }

    public abstract double calArea();

    public abstract double calPerimeter();
}
