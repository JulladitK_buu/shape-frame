/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.shapeframe;

/**
 *
 * @author acer
 */
public class Square extends Shape {

    private double S2;

    public Square(double S2) {
        super("Square");
        this.S2 = S2;
    }

    public double getS2() {
        return S2;
    }

    public void setS2(double S2) {
        this.S2 = S2;
    }

    @Override
    public double calArea() {
        return S2 * S2;
    }

    @Override
    public double calPerimeter() {
        return S2 + S2 + S2 + S2;
    }

}
