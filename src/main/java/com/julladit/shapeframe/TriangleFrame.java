/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.shapeframe;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class TriangleFrame extends JFrame {

    JLabel lblRadius;
    JTextField txtRadius;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");

        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblRadius = new JLabel("Base : ", JLabel.TRAILING);
        lblRadius.setSize(50, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        this.add(lblRadius);

        lblRadius = new JLabel("Height : ", JLabel.TRAILING);
        lblRadius.setSize(50, 20);
        lblRadius.setLocation(5, 30);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        this.add(lblRadius);

        txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);
        this.add(txtRadius);

        txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 30);
        this.add(txtRadius);

        btnCalculate = new JButton("Calcuate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 20);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle  = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 90);
        lblResult.setBackground(Color.LIGHT_GRAY);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous Class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strRadius = txtRadius.getText();
                    double radius = Double.parseDouble(strRadius);
                    Triangle triangle = new Triangle(radius, radius);
                    lblResult.setText("Triangle = " + String.format("%.2f", triangle.getBase(), triangle.getHeight())
                            + " area =  " + String.format("%.2f", triangle.calArea())
                            + " perimter = " + String.format("%.2f", triangle.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
