/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.shapeframe;

/**
 *
 * @author acer
 */
public class Circle extends Shape {

    private double radis;

    public Circle(double radis) {
        super("Circle");
        this.radis = radis;
    }

    public double getRadis() {
        return radis;
    }

    public void setRadis(double radis) {
        this.radis = radis;
    }

    @Override
    public double calArea() {
        return Math.PI * Math.pow(radis, 2);
    }

    @Override
    public double calPerimeter() {
        return 0.5 * Math.PI * radis;
    }

}
